package sheridan;

public class LoginValidator {
	
	private static int MIN_LENGTH = 6;
	
	public static boolean isValidLoginName(String loginName) {
		return loginName != null && loginName.matches("^(?=[a-zA-Z0-9.]{8,20}$)(?!.*[.]{2})[^.].*[^.]$") ;
	}
	
	public static boolean isValidLoginNameLength( String loginName ) {
		return loginName != null && loginName.length() >= MIN_LENGTH;
	}
}
