package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("jonathan1234"));
	}

	@Test
	public void testisValidLoginException() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName(null));
	}

	@Test
	public void testisValidLoginBoundryIn() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("jonat12"));
	}

	@Test
	public void testisValidLoginBoundryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("1Jon!"));
	}

	@Test
	public void testIsValidLoginLengthRegular() {
		assertTrue("Invalid login Length", LoginValidator.isValidLoginNameLength("Jonathan11"));
	}

	@Test
	public void testisValidLoginLengthException() {
		assertFalse("Invalid Login Length", LoginValidator.isValidLoginNameLength(""));
	}

	@Test
	public void testisValidLoginLengthBoundryIn() {
		assertTrue("Invalid Login", LoginValidator.isValidLoginNameLength("Jon 1235"));
	}

	@Test
	public void testisValidLoginLengthBoundryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginNameLength("Jon11"));
	}

}
